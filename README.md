# Finerio React Test App

Aplicación de prueba con Apis de Finerio, desarrollado en React.js

### Pre-requisitos

1. [Node.js](https://nodejs.org/es/)
2. [Yarn](https://yarnpkg.com/)(opcional)
## Instalación

### Con Yarn
```bash
yarn install
```
### Con Npm
```bash
npm install
```


## Ejecución

### Con Yarn
```bash
yarn start
```
### Con Npm
```bash
npm start
```

La aplicación se ejecutará en modo desarrollador.\
Abre [http://localhost:3000](http://localhost:3000) en el navegador para visualizar la aplicación.
