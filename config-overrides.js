const { alias } = require('react-app-rewire-alias');

module.exports = function override(config) {
	alias({
		'@api': 'src/api',
		'@components': 'src/components',
		'@helpers': 'src/utils/helpers',
		'@modules': 'src/modules',
		'@res': 'src/res',
		'@utils': 'src/utils',
	})(config);

	return config;
};
