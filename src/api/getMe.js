import API from './';
const getMe = () => API.get('/me');
export default getMe;
