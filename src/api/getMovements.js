import API from './';
const getMovements = (userId, offset = 0, max = 35) =>
	API.get(
		`/users/${userId}/movements?deep=true&offset=${offset}&max=${max}&includeCharges=true&includeDeposits=true&includeDuplicates=true`
	);
export default getMovements;
