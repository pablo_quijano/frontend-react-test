import axios from 'axios';

import { getAuthToken, logoutActions } from '@helpers';

const API = axios.create({
	baseURL: process.env.REACT_APP_API_URL,
});

API.interceptors.request.use((req) => {
	req.headers['Content-Type'] = 'application/json';
	const authToken = getAuthToken();
	if (authToken) {
		req.headers['Authorization'] = authToken;
	}

	return req;
});

API.interceptors.response.use(
	(res) => res,
	(error) => {
		if (
			error.response.status === 401 &&
			window.location.pathname !== '/login'
		) {
			logoutActions();
		}

		if (error.response.status === 403) {
			window.location.replace('/login');
		}

		return Promise.reject(error);
	}
);

export default API;
