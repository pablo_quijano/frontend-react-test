export const logoutActions = () => {
	localStorage.clear();
	window.location.replace('/login');
};

export const getAuthToken = () => {
	const accessToken = localStorage.getItem('access_token');
	const tokenType = localStorage.getItem('token_type');
	if (!accessToken || !tokenType) {
		return null;
	}
	return `${tokenType} ${accessToken}`;
};
