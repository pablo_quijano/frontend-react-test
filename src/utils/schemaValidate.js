import set from 'lodash/set';

export default (schema) => (values) => {
	try {
		schema.validateSync(values, { abortEarly: false });
		return {};
	} catch (errors) {
		return errors.inner.reduce((acc, current) => {
			let tempObject = { ...acc };
			set(tempObject, current.path, current.message);
			return tempObject;
		}, {});
	}
};
