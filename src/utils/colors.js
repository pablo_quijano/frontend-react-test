export const ERROR_COLOR = '#ff1744';
export const PRIMARY_COLOR = '#283593';
export const PRIMARY_LIGHT_COLOR = '#5f5fc4';
export const SECONDARY_COLOR = '#039be5';
export const SECONDARY_DARK_COLOR = '#006db3';
export const TEXT_COLOR = '#2c2c2c';
