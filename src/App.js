import main from './modules/main';

function App() {
	return <main.Container />;
}

export default App;
