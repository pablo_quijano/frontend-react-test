import PropTypes from 'prop-types';
import { Field } from 'react-final-form';
import styled from 'styled-components';

import { ERROR_COLOR, PRIMARY_LIGHT_COLOR, TEXT_COLOR } from '@utils/colors';

const Wrapper = styled.div`
	display: block;
	position: relative;
	margin: ${({ margin }) => margin};
	input:placeholder-shown:not(:focus) + .label {
		font-size: 16px;
		top: 20px;
		color: #838383;
	}

	input:focus ~ .label {
		color: ${({ highlightColor }) => highlightColor};
	}

	&.with-error .label {
		color: ${({ errorColor }) => errorColor};
	}

	&.with-error input:not(:focus),
	&.with-error input:hover:not(:placeholder-shown):not(:focus) {
		border-bottom: 2px solid ${({ errorColor }) => errorColor} !important;
	}
`;

const Input = styled.input`
	padding: 16px 0 8px 0;
	width: 100%;
	border: 0;
	border-radius: 0;
	box-shadow: none;
	background-color: initial;
	color: ${({ textColor }) => textColor};
	caret-color: ${({ highlightColor }) => highlightColor};
	border-bottom: 2px solid #8a8a8a;
	font-size: 14px;
	line-height: 1.15;
	margin: 0;
	outline: none;
	&:hover:not(:disabled):not(:focus) {
		border-bottom: 2px solid #1e1e1e !important;
	}
	&:focus {
		border-bottom: 2px solid ${({ highlightColor }) => highlightColor};
		transition: all 0.2s;
	}
	&:placeholder-shown:not(:focus) {
		border-bottom: 2px solid #8a8a8a;
	}
	&:disabled {
		border-bottom: 2px dotted #8a8a8a !important;
		pointer-events: none;
		background-color: inherit;
	}
`;

const Label = styled.span`
	position: absolute;
	left: 0;
	top: 0;
	cursor: text;
	font-size: 12px;
	color: #6d6d6d;
	transition: all 0.2s;
	pointer-events: none;
`;

const Error = styled.span`
	font-size: 12px;
	padding-top: 8px;
	line-height: 1;
	color: ${({ errorColor }) => errorColor};
`;

const StyledInput = ({
	disabled,
	errorColor = ERROR_COLOR,
	highlightColor = PRIMARY_LIGHT_COLOR,
	label = '',
	margin = '0',
	name = '',
	textColor = TEXT_COLOR,
	type = 'text',
}) => {
	return (
		<Field name={name}>
			{({ input, meta }) => {
				return (
					<Wrapper
						className={meta.error && meta.touched && 'with-error'}
						margin={margin}
						errorColor={errorColor}
						highlightColor={highlightColor}
					>
						<Input
							{...input}
							disabled={disabled}
							errorColor={errorColor}
							highlightColor={highlightColor}
							placeholder=" "
							textColor={textColor}
							type={type}
						/>
						<Label className="label">{label}</Label>
						{meta.error && meta.touched && (
							<Error className="error" errorColor={errorColor}>
								{meta.error}
							</Error>
						)}
					</Wrapper>
				);
			}}
		</Field>
	);
};

StyledInput.propTypes = {
	disabled: PropTypes.bool,
	errorColor: PropTypes.string,
	highlightColor: PropTypes.string,
	label: PropTypes.string,
	margin: PropTypes.string,
	name: PropTypes.string,
	textColor: PropTypes.string,
	type: PropTypes.string,
};

export default StyledInput;
