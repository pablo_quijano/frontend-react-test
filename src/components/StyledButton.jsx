import PropTypes from 'prop-types';
import styled from 'styled-components';

import { SECONDARY_COLOR, SECONDARY_DARK_COLOR } from '@utils/colors';

const Button = styled.button`
	position: relative;
	display: block;
	margin: 30px auto;
	padding: 0;
	overflow: hidden;
	border-width: 0;
	outline: none;
	border-radius: 2px;
	box-shadow: 0 1px 4px rgba(0, 0, 0, 0.6);
	background-color: ${({ bgColor }) => bgColor};
	transition: background-color 0.3s;
	cursor: pointer;
	margin: ${({ margin }) => margin};
	${({ width }) => width && `width: ${width};`}

	&:hover,
	&:focus {
		background-color: ${({ bgColorHover }) => bgColorHover};
	}

	&:before {
		content: '';

		position: absolute;
		top: 50%;
		left: 50%;

		display: block;
		width: 0;
		padding-top: 0;

		border-radius: 100%;

		background-color: rgba(236, 240, 241, 0.3);

		-webkit-transform: translate(-50%, -50%);
		-moz-transform: translate(-50%, -50%);
		-ms-transform: translate(-50%, -50%);
		-o-transform: translate(-50%, -50%);
		transform: translate(-50%, -50%);
	}

	&:active:before {
		width: 120%;
		padding-top: 120%;
		transition: width 0.2s ease-out, padding-top 0.2s ease-out;
	}
	&.disabled {
		pointer-events: none;
		background-color: #bbb;
		cursor: default;
	}
`;

const Text = styled.span`
	position: relative;
	display: block;
	padding: 12px 24px;
	color: ${({ textColor }) => textColor};
	user-select: none;
`;

const StyledButton = ({
	bgColor = SECONDARY_COLOR,
	bgColorHover = SECONDARY_DARK_COLOR,
	disabled,
	handleClick = () => {},
	margin = '0',
	text = '',
	textColor = '#ecf0f1',
	type = 'button',
	width,
}) => {
	return (
		<Button
			bgColor={bgColor}
			bgColorHover={bgColorHover}
			className={disabled && 'disabled'}
			margin={margin}
			onClick={handleClick}
			type={type}
			width={width}
		>
			<Text textColor={textColor}>{text}</Text>
		</Button>
	);
};

StyledButton.propTypes = {
	bgColor: PropTypes.string,
	bgColorHover: PropTypes.string,
	disabled: PropTypes.bool,
	handleClick: PropTypes.func,
	margin: PropTypes.string,
	text: PropTypes.string,
	textColor: PropTypes.string,
	type: PropTypes.string,
	width: PropTypes.string,
};

export default StyledButton;
