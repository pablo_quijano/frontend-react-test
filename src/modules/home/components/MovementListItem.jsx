import PropTypes from 'prop-types';
import { useMemo } from 'react';
import styled from 'styled-components';

import { ERROR_COLOR, PRIMARY_LIGHT_COLOR, TEXT_COLOR } from '@utils/colors';
import { media } from '@utils/mixins';

import { CHARGE_TYPE } from '../constants';

const Container = styled.div`
	display: flex;
	align-items: center;
	position: relative;
	min-height: 64px;
	margin: 12px 0;
	background-color: #fff;
	border-radius: 2px;
	box-shadow: 0 1px 3px rgb(0 0 0 / 12%), 0 1px 2px rgb(0 0 0 / 24%);
	padding: 0 12px;
	${media.sm`
		margin: 16px 0;
    `}
	${media.md`
		min-height: 80px;
		padding: 0 16px;
		margin: 20px 0;
		border-radius: 4px;
    `}
`;

const DateText = styled.span`
	position: absolute;
	min-height: 12px;
	color: ${TEXT_COLOR};
	font-size: 10px;
	line-height: 12px;
	color: #fff;
	padding: 1px 3px;
	top: 0;
	left: 0;
	border-radius: 2px 0 2px 0;
	background-color: ${PRIMARY_LIGHT_COLOR};
	${media.md`
		min-height: 13px;
		font-size: 11px;
		line-height: 13px;
		border-radius: 4px 0 4px 0;
		padding: 2px 4px;
		
    `}
`;

const Description = styled.span`
	flex-grow: 1;
	min-height: 18px;
	color: ${TEXT_COLOR};
	font-size: 16px;
	line-height: 18px;
	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
	${media.md`
		min-height: 20px;
		font-size: 18px;
		line-height: 20px;
    `}
	${media.lg`
		min-height: 22px;
		font-size: 20px;
		line-height: 22px;
    `}
`;

const Amount = styled.span`
	width: 150px;
	min-width: 150px;
	min-height: 18px;
	color: ${({ type }) => (type === CHARGE_TYPE ? ERROR_COLOR : '#689f38')};
	font-size: 16px;
	line-height: 18px;
	margin-left: 6px;
	text-align: right;
	font-weight: 600;
	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
	${media.md`
		width: 160px;
		min-width: 160px;
		min-height: 20px;
		font-size: 18px;
		line-height: 20px;
		margin-left: 10px;
    `}
	${media.lg`
		width: 170px;
		min-width: 170px;
		min-height: 22px;
		font-size: 20px;
		line-height: 22px;
    `}
`;

const MovementListItem = ({ amount, date, description, type }) => {
	const formattedAmount = useMemo(
		() =>
			new Intl.NumberFormat('es-MX', {
				style: 'currency',
				currency: 'MXN',
				minimumFractionDigits: 0,
				maximumFractionDigits: 0,
			}).format(amount),
		[amount]
	);
	const formattedDate = useMemo(() => {
		const parsedDate = new Date(date);
		return new Intl.DateTimeFormat('es-MX').format(parsedDate);
	}, [date]);
	return (
		<Container>
			<DateText>{formattedDate}</DateText>
			<Description title={description}>{description}</Description>
			<Amount type={type} title={formattedAmount}>
				{formattedAmount}
			</Amount>
		</Container>
	);
};

MovementListItem.propTypes = {
	amount: PropTypes.number,
	date: PropTypes.string,
	description: PropTypes.string,
	type: PropTypes.string,
};

export default MovementListItem;
