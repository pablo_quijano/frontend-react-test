import PropTypes from 'prop-types';
import styled from 'styled-components';

import { logoutActions } from '@helpers';

import { ReactComponent as PowerOffIcon } from '@res/power_off.svg';

import { PRIMARY_COLOR } from '@utils/colors';
import { media } from '@utils/mixins';

const Container = styled.header`
	position: sticky;
	top: 0;
	display: flex;
	width: 100%;
	height: 50px;
	background-color: ${PRIMARY_COLOR};
	box-shadow: 0 3px 6px rgb(0 0 0 / 16%), 0 3px 6px rgb(0 0 0 / 23%);
	z-index: 2;

	${media.sm`
        height: 64px;
    `}
	${media.md`
        height: 80px;
    `}
    ${media.lg`
        height: 100px;
    `}
`;

const Content = styled.div`
	display: flex;
	align-items: center;
	width: calc(100% - 48px);
	height: 26px;
	margin: 12px auto;
	${media.sm`
        width: 500px;
        height: 34px;
        margin: 16px auto;
    `}
	${media.md`
		width: 700px;
        height: 42px;
        margin: 19px auto;
    `}
    ${media.lg`
		width: 900px;
        height: 50px;
        margin: 25px auto;
    `}
    ${media.xl`
		width: 1100px;
    `}
`;

const Text = styled.span`
	flex-grow: 1;
	min-height: 26px;
	color: #fff;
	font-size: 24px;
	line-height: 26px;
	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
	${media.sm`
        min-height: 34px;
        font-size: 32px;
        line-height: 34px;
    `}
	${media.md`
        min-height: 42px;
        font-size: 40px;
        line-height: 42px;
    `}
    ${media.lg`
        min-height: 50px;
        font-size: 48px;
        line-height: 50px;
    `}
`;

const StyledPowerOffIcon = styled(PowerOffIcon)`
	width: 20px;
	min-width: 20px;
	height: 20px;
	cursor: pointer;
	margin-left: 10px;
	path {
		fill: #fff;
	}
	${media.sm`
        width: 24px;
        min-width: 24px;
        height: 24px;
    `}
	${media.md`
        width: 28px;
        min-width: 28px;
        height: 28px;
    `}
    ${media.lg`
        width: 32px;
        min-width: 32px;
        height: 32px;
    `}
`;
const Header = ({ userName }) => {
	return (
		<Container>
			<Content>
				<Text title={`Hola ${userName || ''}`}>
					Hola {userName || ''}
				</Text>
				<StyledPowerOffIcon onClick={logoutActions} />
			</Content>
		</Container>
	);
};

Header.propTypes = {
	userName: PropTypes.string,
};

export default Header;
