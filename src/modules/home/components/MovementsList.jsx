import PropTypes from 'prop-types';
import { useCallback, useState } from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import styled from 'styled-components';

import { media } from '@utils/mixins';

import * as actions from '../actions';
import * as selectors from '../selectors';

import Header from './Header';
import MovementListItem from './MovementListItem';

const Container = styled.div`
	display: flex;
	flex-direction: column;
	width: 100%;
	height: 100vh;
	overflow: auto;
	z-index: 1;
	& > div {
		display: flex;
		flex-direction: column;
		margin: 8px auto;
		width: calc(100% - 48px);
		${media.sm`
			width: 500px;
		`}
		${media.md`
			margin: 12px auto;
			width: 700px;
		`}
		${media.lg`
			width: 900px;
		`}
		${media.xl`
			width: 1100px;
		`}
	}
`;

const LoadingText = styled.span`
	min-height: 26px;
	color: #ccc;
	font-size: 24px;
	font-weight: 100;
	line-height: 26px;
	margin: 12px 0;
	${media.sm`
        min-height: 34px;
        font-size: 32px;
        line-height: 34px;
		margin: 16px 0;
    `}
	${media.md`
        min-height: 42px;
        font-size: 40px;
        line-height: 42px;
		margin: 20px 0;
    `}
    ${media.lg`
        min-height: 50px;
        font-size: 48px;
        line-height: 50px;
    `}
`;

const MovementsList = ({
	fetchMovements,
	hasMoreMovements,
	isFetchingMovements,
	movements,
	userId,
	userName,
}) => {
	const [currentPage, setNextPage] = useState(0);
	const handleLoadMore = useCallback(() => {
		if (!isFetchingMovements) {
			fetchMovements(userId, currentPage);
			setNextPage(currentPage + 1);
		}
	}, [currentPage, isFetchingMovements, userId]);

	if (!userId) {
		return null;
	}
	return (
		<Container>
			<Header userName={userName} />
			<InfiniteScroll
				loadMore={handleLoadMore}
				hasMore={hasMoreMovements}
				loader={<LoadingText key={0}>Cargando...</LoadingText>}
				useWindow={false}
				threshold={10}
			>
				{movements.map((movement) => (
					<MovementListItem
						amount={movement.amount}
						date={movement.date}
						description={movement.description}
						key={`movement-list-item-${movement.id}`}
						type={movement.type}
					/>
				))}
			</InfiniteScroll>
		</Container>
	);
};

const mapStateToProps = createStructuredSelector({
	hasMoreMovements: selectors.getHasMoreMovements,
	isFetchingMovements: selectors.getIsFetchingMovements,
	movements: selectors.getFetchMovementsData,
});

const mapDispatchToProps = (dispatch) => ({
	fetchMovements: (userId, page) =>
		dispatch(actions.fetchMovements(userId, page)),
});

MovementsList.propTypes = {
	fetchMovements: PropTypes.func,
	hasMoreMovements: PropTypes.bool,
	isFetchingMovements: PropTypes.bool,
	movements: PropTypes.array,
	userId: PropTypes.string,
	userName: PropTypes.string,
};

export default connect(mapStateToProps, mapDispatchToProps)(MovementsList);
