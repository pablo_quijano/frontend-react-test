export const NAME = 'home';

export const MOVEMENTS_BY_PAGE = 10;
export const DEPOSIT_TYPE = 'DEPOSIT';
export const CHARGE_TYPE = 'CHARGE';
