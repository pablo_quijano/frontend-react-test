import getMe from '@api/getMe';

import { FETCH_ME, FETCH_ME_FAILURE, FETCH_ME_SUCCESS } from '../actionTypes';

export default () => (dispatch) => {
	dispatch({ type: FETCH_ME });

	getMe()
		.then((response) => {
			dispatch({
				type: FETCH_ME_SUCCESS,
				payload: { id: response.data.id, name: response.data.name },
			});
		})
		.catch(() => {
			dispatch({
				type: FETCH_ME_FAILURE,
			});
		});
};
