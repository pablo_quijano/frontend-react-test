import getMovements from '@api/getMovements';

import {
	FETCH_MOVEMENTS,
	FETCH_MOVEMENTS_FAILURE,
	FETCH_MOVEMENTS_SUCCESS,
} from '../actionTypes';
import { MOVEMENTS_BY_PAGE } from '../constants';

export default (userId, page) => (dispatch) => {
	dispatch({ type: FETCH_MOVEMENTS });

	getMovements(userId, page * MOVEMENTS_BY_PAGE, MOVEMENTS_BY_PAGE)
		.then((response) => {
			dispatch({
				type: FETCH_MOVEMENTS_SUCCESS,
				payload: response.data.data,
			});
		})
		.catch(() => {
			dispatch({
				type: FETCH_MOVEMENTS_FAILURE,
			});
		});
};
