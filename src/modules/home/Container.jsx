import PropTypes from 'prop-types';
import { useEffect } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import styled from 'styled-components';

import * as actions from './actions';
import MovementsList from './components/MovementsList';
import * as selectors from './selectors';

const Container = styled.div`
	display: flex;
	flex-direction: column;
	width: 100%;
`;

const HomeContainer = ({ fetchMe, reset, userData }) => {
	useEffect(() => {
		fetchMe();
		return () => reset();
	}, []);
	return (
		<Container>
			<MovementsList
				userId={userData && userData.id}
				userName={userData && userData.name}
			/>
		</Container>
	);
};

const mapStateToProps = createStructuredSelector({
	isFetchingMe: selectors.getIsFetchingMe,
	userData: selectors.getFetchMeData,
});

const mapDispatchToProps = (dispatch) => ({
	fetchMe: () => dispatch(actions.fetchMe()),
	reset: () => dispatch(actions.reset()),
});

HomeContainer.propTypes = {
	isFetchingMe: PropTypes.bool,
	fetchMe: PropTypes.func,
	reset: PropTypes.func,
	userData: PropTypes.object,
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer);
