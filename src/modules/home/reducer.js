import update from 'immutability-helper';

import {
	FETCH_ME,
	FETCH_ME_FAILURE,
	FETCH_ME_SUCCESS,
	FETCH_MOVEMENTS,
	FETCH_MOVEMENTS_FAILURE,
	FETCH_MOVEMENTS_SUCCESS,
	RESET,
} from './actionTypes';

const INITIAL_STATE = {
	fetchMe: {
		data: null,
		isFetching: false,
	},
	fetchMovements: {
		data: [],
		isFetching: false,
	},
	hasMoreMovements: true,
};

export default function (state = INITIAL_STATE, action) {
	switch (action.type) {
		case FETCH_ME:
			return update(state, { fetchMe: { isFetching: { $set: true } } });

		case FETCH_ME_FAILURE:
			return update(state, { fetchMe: { isFetching: { $set: false } } });

		case FETCH_ME_SUCCESS:
			return update(state, {
				fetchMe: {
					$merge: {
						data: action.payload,
						isFetching: false,
					},
				},
			});

		case FETCH_MOVEMENTS:
			return update(state, {
				fetchMovements: { isFetching: { $set: true } },
			});

		case FETCH_MOVEMENTS_FAILURE:
			return update(state, {
				fetchMovements: { isFetching: { $set: false } },
				hasMoreMovements: {
					$set: false,
				},
			});

		case FETCH_MOVEMENTS_SUCCESS:
			if (action.payload.length) {
				return update(state, {
					fetchMovements: {
						$merge: {
							data: [
								...state.fetchMovements.data,
								...action.payload,
							],
							isFetching: false,
						},
					},
				});
			}
			return update(state, {
				hasMoreMovements: {
					$set: false,
				},
			});
		case RESET:
			return update(state, {
				$set: INITIAL_STATE,
			});

		default:
			return state;
	}
}
