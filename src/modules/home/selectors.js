import { createSelector } from 'reselect';

import { NAME } from './constants';

export const getModel = (state) => state[NAME];

export const getFetchMe = createSelector(getModel, (model) => model.fetchMe);

export const getIsFetchingMe = createSelector(
	getFetchMe,
	(fetchMe) => fetchMe.isFetching
);

export const getFetchMeData = createSelector(
	getFetchMe,
	(fetchMe) => fetchMe.data
);

export const getFetchMovements = createSelector(
	getModel,
	(model) => model.fetchMovements
);

export const getIsFetchingMovements = createSelector(
	getFetchMovements,
	(fetchMovements) => fetchMovements.isFetching
);

export const getFetchMovementsData = createSelector(
	getFetchMovements,
	(fetchMovements) => fetchMovements.data
);

export const getHasMoreMovements = createSelector(
	getModel,
	(model) => model.hasMoreMovements
);
