import { object, string } from 'yup';

import schemaValidate from '@utils/schemaValidate';

import { EMAIL_REGEX } from './constants';

export default schemaValidate(
	object().shape({
		username: string()
			.required('Este campo es requerido')
			.matches(EMAIL_REGEX, 'Dirección de e-mail inválida')
			.nullable(),
		password: string().required('Este campo es requerido').nullable(),
	})
);
