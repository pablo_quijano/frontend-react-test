import { createSelector } from 'reselect';

import { NAME } from './constants';

export const getModel = (state) => state[NAME];

export const getInitialValues = createSelector(
	getModel,
	(model) => model.initialValues
);

export const getLogin = createSelector(getModel, (model) => model.login);

export const getIsLoggingIn = createSelector(
	getLogin,
	(login) => login.isLoggingIn
);

export const getLoginErrors = createSelector(getLogin, (login) => login.errors);
