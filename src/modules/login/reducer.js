import update from 'immutability-helper';

import { LOGIN, LOGIN_FAILURE, LOGIN_SUCCESS, RESET } from './actionTypes';

const INITIAL_STATE = {
	initialValues: {
		password: '',
		username: '',
	},
	login: {
		isLoggingIn: false,
		errors: null,
	},
};

export default function (state = INITIAL_STATE, action) {
	switch (action.type) {
		case LOGIN:
			return update(state, {
				login: {
					$merge: { isLoggingIn: true, errors: null },
				},
			});

		case LOGIN_FAILURE:
			return update(state, {
				login: {
					$merge: { isLoggingIn: false, errors: action.payload },
				},
			});

		case LOGIN_SUCCESS:
			return update(state, {
				login: { isLoggingIn: { $set: false } },
			});

		case RESET:
			return update(state, {
				$set: INITIAL_STATE,
			});

		default:
			return state;
	}
}
