import PropTypes from 'prop-types';
import { useEffect } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';

import { PRIMARY_COLOR } from '@utils/colors';
import { media } from '@utils/mixins';

import * as actions from './actions';
import LoginForm from './components/LoginForm';

const Container = styled.div`
	position: absolute;
	display: flex;
	width: 100%;
	height: 100%;
	background-color: ${PRIMARY_COLOR};
	overflow: auto;
`;

const Card = styled.div`
	display: flex;
	flex-direction: column;
	width: 100%;
	background-color: #fff;
	padding: 40px 36px;
	margin: auto;
	${media.sm`
		width: 360px;
		border-radius: 6px;
		box-shadow: 0 14px 28px rgb(0 0 0 / 25%), 0 10px 10px rgb(0 0 0 / 22%);
    `}
	${media.md`
		width: 528px;
    `}
`;

const Title = styled.h1`
	min-height: 32px;
	color: ${PRIMARY_COLOR};
	font-size: 30px;
	font-weight: 600;
	line-height: 32px;
	position: relative;

	&:before {
		background-color: ${PRIMARY_COLOR};
		content: '';
		display: block;
		height: 32px;
		left: -36px;
		margin-top: -16px;
		position: absolute;
		top: 50%;
		width: 5px;
	}
`;

const LoginContainer = ({ reset }) => {
	useEffect(() => () => reset(), []);
	return (
		<Container>
			<Card>
				<Title>INICIAR SESIÓN</Title>
				<LoginForm />
			</Card>
		</Container>
	);
};

const mapDispatchToProps = (dispatch) => ({
	reset: () => dispatch(actions.reset()),
});

LoginContainer.propTypes = {
	reset: PropTypes.func,
};

export default connect(null, mapDispatchToProps)(LoginContainer);
