import { RESET } from '../actionTypes';

export default () => ({ type: RESET });
