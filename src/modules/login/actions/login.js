import login from '@api/login';

import { SERVER_ERROR_MESSAGE } from '@utils/constants';

import { LOGIN, LOGIN_FAILURE, LOGIN_SUCCESS } from '../actionTypes';
import { AUTH_ERROR_MESSAGE } from '../constants';

export default (data, goToHome) => (dispatch) => {
	dispatch({ type: LOGIN });

	login(data)
		.then((response) => {
			if (
				!response.data ||
				!response.data.access_token ||
				!response.data.token_type
			) {
				dispatch({
					type: LOGIN_FAILURE,
					payload: AUTH_ERROR_MESSAGE,
				});

				return;
			}
			localStorage.setItem('access_token', response.data.access_token);
			localStorage.setItem('token_type', response.data.token_type);
			dispatch({
				type: LOGIN_SUCCESS,
			});
			goToHome();
		})
		.catch((error) => {
			dispatch({
				type: LOGIN_FAILURE,
				payload:
					error.response.status === 401
						? AUTH_ERROR_MESSAGE
						: SERVER_ERROR_MESSAGE,
			});
		});
};
