import { NAME } from './constants';

export const LOGIN = `${NAME}/LOGIN`;
export const LOGIN_FAILURE = `${NAME}/LOGIN_FAILURE`;
export const LOGIN_SUCCESS = `${NAME}/LOGIN_SUCCESS`;

export const RESET = `${NAME}/RESET`;
