import PropTypes from 'prop-types';
import styled from 'styled-components';

import { ERROR_COLOR } from '@utils/colors';

const Container = styled.div`
	display: flex;
	padding: 10px 16px;
	background-color: #ffebee;
	border: 1px solid ${ERROR_COLOR};
	border-radius: 4px;
	overflow: hidden;
	transition: all 0.3s linear;
	&.hidden {
		height: 0;
		border: 0;
		padding: 0 16px;
	}
`;

const Text = styled.span`
	width: 100%;
	font-size: 12px;
	color: ${ERROR_COLOR};
`;

const ErrorMessage = ({ message }) => {
	return (
		<Container className={message === null && 'hidden'}>
			<Text>{message}</Text>
		</Container>
	);
};

ErrorMessage.propTypes = {
	message: PropTypes.string,
};

export default ErrorMessage;
