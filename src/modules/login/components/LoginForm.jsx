import PropTypes from 'prop-types';
import { Form } from 'react-final-form';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';
import { createStructuredSelector } from 'reselect';

import { StyledButton, StyledInput } from '@components';

import * as actions from '../actions';
import * as selectors from '../selectors';
import validate from '../validate';

import ErrorMessage from './ErrorMessage';

const LoginForm = ({
	history,
	initialValues,
	isLoggingIn,
	login,
	loginErrors,
}) => {
	return (
		<Form
			initialValues={initialValues}
			onSubmit={(values) => login(values, () => history.push('/'))}
			validate={validate}
		>
			{({ handleSubmit }) => (
				<form onSubmit={handleSubmit}>
					<StyledInput
						disabled={isLoggingIn}
						label="Email"
						margin="20px 0 42px 0"
						name="username"
						type="email"
					/>
					<StyledInput
						disabled={isLoggingIn}
						label="Contraseña"
						margin="20px 0 32px 0"
						name="password"
						type="password"
					/>
					<ErrorMessage message={loginErrors} />
					<StyledButton
						disabled={isLoggingIn}
						margin="16px 0 0 0"
						text={isLoggingIn ? 'Iniciando...' : 'Entrar'}
						type="submit"
						width="100%"
					/>
				</form>
			)}
		</Form>
	);
};

const mapStateToProps = createStructuredSelector({
	isLoggingIn: selectors.getIsLoggingIn,
	initialValues: selectors.getInitialValues,
	loginErrors: selectors.getLoginErrors,
});

const mapDispatchToProps = (dispatch) => ({
	login: (data, goToHome) => dispatch(actions.login(data, goToHome)),
});

LoginForm.propTypes = {
	history: PropTypes.object,
	initialValues: PropTypes.object,
	isLoggingIn: PropTypes.bool,
	login: PropTypes.func,
	loginErrors: PropTypes.string,
};

export default compose(
	connect(mapStateToProps, mapDispatchToProps),
	withRouter
)(LoginForm);
