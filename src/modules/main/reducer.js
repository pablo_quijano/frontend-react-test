import { applyMiddleware, combineReducers, createStore } from 'redux';
import thunk from 'redux-thunk';

import home from '@modules/home';
import login from '@modules/login';

export default function configureStore() {
	const rootReducer = combineReducers({
		[home.NAME]: home.reducer,
		[login.NAME]: login.reducer,
	});

	return createStore(rootReducer, {}, applyMiddleware(thunk));
}
