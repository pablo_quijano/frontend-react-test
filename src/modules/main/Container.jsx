import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import home from '@modules/home';
import login from '@modules/login';

import LoginRoute from './components/LoginRoute';
import PageNotFound from './components/PageNotFound';
import PrivateRoute from './components/PrivateRoute';

const MainContainer = () => {
	return (
		<Router>
			<Switch>
				<PrivateRoute exact path="/">
					<home.Container />
				</PrivateRoute>
				<LoginRoute path="/login">
					<login.Container />
				</LoginRoute>
				<Route path="*">
					<PageNotFound />
				</Route>
			</Switch>
		</Router>
	);
};

export default MainContainer;
