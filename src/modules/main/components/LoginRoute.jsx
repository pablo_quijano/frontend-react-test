import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';

import { getAuthToken } from '@helpers';

const LoginRoute = ({ children, ...rest }) => {
	const hasAccess = getAuthToken() !== null;
	if (hasAccess) {
		return <Redirect to={{ pathname: '/' }} />;
	}
	return <Route {...rest} render={() => children} />;
};

LoginRoute.propTypes = {
	children: PropTypes.element,
};

export default LoginRoute;
