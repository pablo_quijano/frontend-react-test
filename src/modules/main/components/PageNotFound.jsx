import styled from 'styled-components';

import { PRIMARY_COLOR, SECONDARY_COLOR } from '@utils/colors';
import { media } from '@utils/mixins';

const Container = styled.div`
	position: absolute;
	display: flex;
	width: 100%;
	height: 100%;
	overflow: auto;
`;

const Content = styled.div`
	display: flex;
	flex-direction: column;
	width: 100%;
	margin: auto;
	${media.sm`
		width: 360px;
    `}
	${media.md`
		width: 528px;
    `}
`;

const Title = styled.h1`
	min-height: 84px;
	color: ${PRIMARY_COLOR};
	font-size: 82px;
	font-weight: 600;
	line-height: 84px;
	margin: 0;
	text-align: center;
	${media.sm`
		min-height: 112px;
		font-size: 110px;
		line-height: 112px;
    `}
	${media.md`
		min-height: 160px;
		font-size: 158px;
		line-height: 160px;
    `}
`;

const Message = styled.p`
	min-height: 16px;
	color: ${SECONDARY_COLOR};
	font-size: 14px;
	font-weight: 500;
	line-height: 16px;
	margin: 0;
	text-align: center;
	${media.sm`
		min-height: 22px;
		font-size: 20px;
		line-height: 22px;
    `}
	${media.md`
		min-height: 32px;
		font-size: 30px;
		line-height: 32px;
    `}
`;

const PageNotFound = () => (
	<Container>
		<Content>
			<Title>404</Title>
			<Message>Página no encontrada</Message>
		</Content>
	</Container>
);

export default PageNotFound;
