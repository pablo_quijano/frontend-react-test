import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';

import { getAuthToken } from '@helpers';

const PrivateRoute = ({ children, ...rest }) => {
	const hasAccess = getAuthToken() !== null;
	if (!hasAccess) {
		return <Redirect to={{ pathname: '/login' }} />;
	}
	return <Route {...rest} render={() => children} />;
};

PrivateRoute.propTypes = {
	children: PropTypes.element,
};

export default PrivateRoute;
